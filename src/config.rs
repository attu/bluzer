use anyhow::Result;
use serde::Deserialize;
use std::path::PathBuf;
use std::time::Duration;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(parse(from_os_str))]
    conf_file: PathBuf,
    #[structopt(name = "verbose", short = "v", long, help = "verbose")]
    verbose: bool,
    #[structopt(name = "dry-run", short = "n", long, help = "dry run")]
    dry_run: bool,
}

fn default_attempts() -> u8 {
    1
}

fn default_distance() -> f32 {
    2.0
}

fn default_probing() -> Duration {
    Duration::from_secs(2)
}

fn default_command_args() -> Vec<String> {
    vec![]
}

fn default_transparency() -> f32 {
    2.0
}

#[derive(Debug, PartialEq, Clone, Deserialize)]
pub struct Command {
    pub program: String,
    #[serde(default = "default_command_args")]
    pub args: Vec<String>,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct Action {
    #[serde(default = "default_distance")]
    pub distance: f32,
    pub command: Command,
    #[serde(default = "default_attempts")]
    pub attempts: u8,
}

impl Action {
    #[cfg(test)]
    pub fn new(distance: f32, program: impl ToString, attempts: u8) -> Self {
        Self {
            distance,
            command: Command {
                program: program.to_string(),
                args: vec![],
            },
            attempts,
        }
    }
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct Settings {
    pub mac: String,
    pub lock: Action,
    pub unlock: Action,
    #[serde(default = "default_probing")]
    pub probing: Duration,
    #[serde(default = "default_transparency")]
    pub transparency: f32,
}

#[derive(Debug)]
pub struct Configuration {
    pub verbose: bool,
    pub dry_run: bool,
    pub address: bluez::Address,
    pub lock: Action,
    pub unlock: Action,
    pub probing: Duration,
    pub transparency: f32,
}

impl Configuration {
    pub fn from_args() -> Result<Self> {
        let args = Opt::from_args();
        let f = std::fs::File::open(args.conf_file)?;
        let settings: Settings = serde_yaml::from_reader(f)?;

        Ok(Self {
            verbose: args.verbose,
            dry_run: args.dry_run,
            address: bluez::Address::from_slice(
                settings
                    .mac
                    .split(':')
                    .map(|s| u8::from_str_radix(s, 16))
                    .filter_map(Result::ok)
                    .rev()
                    .collect::<Vec<_>>()
                    .as_slice(),
            ),
            lock: settings.lock,
            unlock: settings.unlock,
            probing: settings.probing,
            transparency: settings.transparency,
        })
    }
}
