mod config;

use anyhow::{anyhow, Result};
use bluez::client::BlueZClient;
use config::{Action, Command, Configuration};
use slog::{debug, info, o, Drain};

fn rssi_at_1m(power_level: i8) -> i8 {
    match power_level {
        1 => -84,
        2 => -81,
        3 => -77,
        4 => -72,
        5 => -69,
        6 => -65,
        7 => -59,
        _ => -115,
    }
}

fn distance(tx: i8, rssi: i8, transparency: f32) -> f32 {
    10_f32.powf((tx - rssi) as f32 / (transparency * 10.0))
}

#[tokio::main]
async fn main() -> Result<()> {
    let conf = Configuration::from_args()?;
    let log_level = if conf.verbose {
        slog::Level::Trace
    } else {
        slog::Level::Info
    };
    let log = create_logger(log_level);
    debug!(log, "{:#?}", conf);

    let probing = conf.probing;
    let mut ctl = StateController::new(State::Unlocked(0), conf.lock, conf.unlock);
    let mut client = BlueZClient::new()?;

    let controllers = client.get_controller_list().await?;
    let controller = controllers
        .first()
        .ok_or_else(|| anyhow!("no controller"))?;

    client
        .add_device(
            *controller,
            conf.address,
            bluez::client::AddressType::LEPublic,
            bluez::client::AddDeviceAction::AutoConnect,
        )
        .await
        .ok();

    let transparency = conf.transparency;
    loop {
        let distance = client
            .get_connection_info(
                *controller,
                conf.address,
                bluez::client::AddressType::LEPublic,
            )
            .await
            .ok()
            .map(|info| info.tx_power.map(rssi_at_1m).zip(info.rssi))
            .flatten()
            .map(|(tx_power, rssi)| {
                let distance = distance(tx_power, rssi, transparency);
                debug!(
                    log,
                    "distance(rssi: {}, tx_power {}, transparency: {}) = {:.2}m",
                    rssi,
                    tx_power,
                    transparency,
                    distance
                );
                distance
            })
            .unwrap_or_else(|| {
                debug!(log, "device unavilable");
                127.0
            });

        if let Some(Command { program, args }) = ctl.update(distance) {
            if conf.dry_run {
                info!(log, "program: {:#?}, args: {:#?}", program, args);
            } else {
                std::process::Command::new(program).args(args).output()?;
            }
        }
        tokio::time::sleep(probing).await;
    }
}

enum State {
    Locked(u8),
    Unlocked(u8),
}

struct StateData {
    lock: Action,
    unlock: Action,
}

impl State {
    fn update(&self, data: &StateData, distance: f32) -> State {
        match self {
            State::Locked(hits) => {
                if distance < data.unlock.distance {
                    let hits = hits + 1;
                    if hits >= data.unlock.attempts {
                        State::Unlocked(0)
                    } else {
                        State::Locked(hits)
                    }
                } else {
                    State::Locked(0)
                }
            }
            State::Unlocked(hits) => {
                if distance > data.lock.distance {
                    let hits = hits + 1;
                    if hits >= data.lock.attempts {
                        State::Locked(0)
                    } else {
                        State::Unlocked(hits)
                    }
                } else {
                    State::Unlocked(0)
                }
            }
        }
    }
}

struct StateController {
    data: StateData,
    state: State,
}

impl StateController {
    fn new(state: State, lock: Action, unlock: Action) -> Self {
        Self {
            data: StateData { lock, unlock },
            state,
        }
    }

    fn update(&mut self, distance: f32) -> Option<Command> {
        let new = self.state.update(&self.data, distance);
        let ret = match (&self.state, &new) {
            (State::Locked(_), State::Unlocked(_)) => Some(self.data.unlock.command.clone()),
            (State::Unlocked(_), State::Locked(_)) => Some(self.data.lock.command.clone()),
            _ => None,
        };
        self.state = new;
        ret
    }
}

pub fn create_logger(level: slog::Level) -> slog::Logger {
    let drain = slog_term::PlainDecorator::new(std::io::stdout());
    let drain = slog_term::CompactFormat::new(drain).build().fuse();
    let drain =
        slog::Filter::new(drain, move |r: &slog::Record| r.level().is_at_least(level)).fuse();
    let drain = slog_async::Async::new(drain);
    let drain = drain.build().fuse();
    slog::Logger::root(drain, o!())
}

#[cfg(test)]
mod test {
    use crate::{Action, Command, State, StateController};

    #[test]
    fn some_test() {
        let lock = Action::new(4.0, "lock", 3);
        let unlock = Action::new(2.0, "unlock", 1);

        let mut ctl = StateController::new(State::Locked(0), lock, unlock);

        assert_eq!(ctl.update(2.2), None);
        assert_eq!(
            ctl.update(1.9),
            Some(Command {
                program: "unlock".into(),
                args: vec![]
            })
        );
        assert_eq!(ctl.update(1.9), None);
        assert_eq!(ctl.update(3.8), None);
        assert_eq!(ctl.update(4.1), None);
        assert_eq!(ctl.update(4.1), None);
        assert_eq!(
            ctl.update(4.1),
            Some(Command {
                program: "lock".into(),
                args: vec![]
            })
        );
        assert_eq!(ctl.update(4.1), None);
    }
}
